public class HelloWorld{

    public static void main(String[] args) {
       Account[] arrayOfAccounts = new Account[5];

       double[] amounts = {23,5444,2,345,34};
       String[] names = {"Picard", "Ryker", "wolf", "Troy", "Data"};

       for (int i = 1; i < arrayOfAccounts.length; i++){
           arrayOfAccounts[i].setAmount(amounts[i]);
           arrayOfAccounts[i].setName(names[i]);

           System.out.println(arrayOfAccounts[i].getName() + ":" + arrayOfAccounts[i].getAmount());

           arrayOfAccounts[i].addInterest();

           System.out.println("add interest " + arrayOfAccounts[i].getName() + ":" + arrayOfAccounts[i].getAmount());
      
        }

    }




}